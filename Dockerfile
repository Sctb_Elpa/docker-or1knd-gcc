# Make wrapper
#
# Based on https://ownyourbits.com/2017/06/20/c-build-environment-in-a-docker-container/
#
# Usage:
#   It is recommended to use this alias
#     alias xmake='docker run --rm -v "$(pwd):/src" -t sctb-elpa/or1knd-gcc'
#   Then, use it just as you would use 'make'
#
# Note: you can leave the container running for faster execution. Use these aliases
#     alias runmake='docker run --rm -d -v "$(pwd):/src" -e PROJECT_BASE=$(pwd) \
#	--entrypoint /bg.sh -t --name or1knd-gcc-mmake sctb-elpa/or1knd-gcc'
#     alias mmake='docker exec -t or1knd-gcc-mmake /mmake.sh $(pwd)'
#     alias mcmake='docker exec -t or1knd-gcc-mmake /mcmake.sh $(pwd)'
#

# build toolchain
FROM vookimedlo/debian-gcc:gcc_debian-stable AS build

ARG build_jobs=1

# install build dependencies
RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        bison \
        flex \
        libgmp-dev \
        libmpc-dev \
        libmpfr-dev \
        texinfo \
	wget

ENV     TARGET=or1knd-elf \
        PREFIX=/opt/or1knd-elf \
        BUILD_DIR=/tmp/build
ENV     PATH="${PREFIX}/bin:${PATH}"
        
RUN mkdir ${BUILD_DIR}
WORKDIR ${BUILD_DIR}

# binutils
RUN git clone https://github.com/openrisc/binutils-gdb.git --depth 1 --branch gdb-7.11-or1k \
	&& mkdir -p binutils-gdb/build && cd binutils-gdb/build \
        && ../configure --target=${TARGET} --prefix=${PREFIX} \
                --enable-shared \
                --disable-itcl \
                --disable-tk \
                --disable-tcl \
                --disable-winsup \
                --disable-libgui \
                --disable-rda \
                --disable-sid \
                --disable-sim \
                --disable-gdb \
                --with-sysroot \
                --disable-newlib \
                --disable-libgloss \
                --disable-werror \
	&& make -j$build_jobs \
	&& make install

# GCC stage 1
RUN git clone https://github.com/openrisc/or1k-gcc.git --depth 1 --branch or1k-5.2.0 \
	&& echo "MULTILIB_EXTRA_OPTS = msoft-mul msoft-div" >> or1k-gcc/gcc/config/or1k/t-or1knd \
        && mkdir -p or1k-gcc/build1 && cd or1k-gcc/build1 \
        && ../configure --target=${TARGET} --prefix=${PREFIX} \
                --enable-languages=c \
                --disable-shared \
                --disable-libssp \
                --disable-werror \
        && make -j$build_jobs \
        && make install

# newlib
RUN git clone https://github.com/openrisc/newlib.git --depth 1 --branch or1k \
        && mkdir -p newlib/build && cd newlib/build \
        && ../configure --target=${TARGET} --prefix=${PREFIX} \
        && make -j$build_jobs \
        && make install

# GCC stage 2
RUN mkdir -p or1k-gcc/build2 && cd or1k-gcc/build2 \
        && ../configure --target=${TARGET} --prefix=${PREFIX} \
                --enable-languages=c --disable-shared --disable-libssp \
                --with-newlib \
        && make -j$build_jobs \
        && make install

# gdb
RUN wget https://github.com/openrisc/newlib/releases/download/v2.3.0-1/or1k-elf_gcc5.2.0_binutils2.26_newlib2.3.0-1_gdb7.11.tgz \
        && tar -xf or1k-elf_gcc5.2.0_binutils2.26_newlib2.3.0-1_gdb7.11.tgz \
                or1k-elf/share/gdb or1k-elf/bin/or1k-elf-gdb

# Deploy
FROM ownyourbits/minidebian
LABEL description="or1knd-gcc Make wrapper"

ENV PATH="/opt/or1knd-elf/bin:/opt/or1k-elf/bin:$PATH" \
    TERM="xterm"

# Install bc & adduser
RUN apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
	libpython2.7 adduser bc make cmake; \
    adduser builder --disabled-password --gecos ""; \
    echo "builder ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers; \
    sed -i "s|^#force_color_prompt=.*|force_color_prompt=yes|" /home/builder/.bashrc; \
    apt-get purge -y adduser passwd; \
    apt-get autoremove -y; apt-get clean; rm /var/lib/apt/lists/* -r; rm -rf /usr/share/man/*

USER builder

COPY --from=build /opt/or1knd-elf /opt/or1knd-elf
COPY --from=build /tmp/build/or1k-elf /opt/or1k-elf

COPY bg.sh mmake.sh mcmake.sh /

WORKDIR /src

ENTRYPOINT /run.sh
