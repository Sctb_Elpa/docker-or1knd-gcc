## Образ, содержащий компилятор gcc для архитектуры or1knd

### Сборка
Запустить в этом каталоге.
```
docker build --build-arg build_jobs=<количество_ядер> -t sctb-elpa/or1knd-gcc  .
```

### Запуск
В каталоге с проектом выполнить
```
docker run --rm -d -v "$(pwd):/src" -e PROJECT_BASE=$(pwd) \
	--entrypoint /bg.sh -t --name or1knd-gcc-mmake sctb-elpa/or1knd-gcc
```
Это запустит  контейнер с компилятором.

Вместо make использовать
```
docker exec -t or1knd-gcc-mmake /mmake.sh $(pwd)
```

Удобно сделать алиас:
```
alias mmake='docker exec -t or1knd-gcc-mmake /mmake.sh $(pwd)'
```

Аналогично cmake:
```
alias mcmake='docker exec -t or1knd-gcc-mmake /mcmake.sh $(pwd)'
```


