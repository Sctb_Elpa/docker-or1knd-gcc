#!/bin/bash

# Copyleft 2017 by Ignacio Nunez Hernanz <nacho _a_t_ ownyourbits _d_o_t_ com>
# GPL licensed (see end of file) * Use at your own risk!
#
# Details at ownyourbits.com

HOST_DIR=$1
ARGS=${@:2}

export TERM="xterm"

function map_dir_host2container()
{
   realpath -m --relative-to ${PROJECT_BASE} ${HOST_DIR}
}

cd /src/`map_dir_host2container ${HOST_DIR}` && cmake $ARGS

