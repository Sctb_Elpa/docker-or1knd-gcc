#!/bin/bash

# Copyleft 2017 by Ignacio Nunez Hernanz <nacho _a_t_ ownyourbits _d_o_t_ com>
# GPL licensed (see end of file) * Use at your own risk!
#
# Details at ownyourbits.com

HOST_DIR=$1
ARGS=${@:2}

export TERM="xterm"

function timecmd()
{
  local BRWN=$'\e[0;33m'
  local YE=$'\e[1;33m'
  local NC=$'\e[0m'
  local START=$( date +%s%N )
  $@
  local END=$( date +%s%N )
  local DELTA=$( expr substr $( echo "( $END - $START )/1000000" | bc -l ) 1 8 )
  echo -e "\n${BRWN}compilation time ${YE}$DELTA ms${NC}"
}

function map_dir_host2container()
{
   realpath -m --relative-to ${PROJECT_BASE} ${HOST_DIR}
}

# COMPILE!
NICE="nice -n19"
timecmd $NICE make $ARGS -C /src/`map_dir_host2container ${HOST_DIR}`

